<?php 
    require_once("global.php");
?>

<!DOCTYPE html">
<html>

<head>    
    <title>Страница для администратора</title>	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
    <div id = "page">
      <?= $header; ?>
        <div id="wrapper">
            <div id="content">

                <h1>Создавай культуру</h1><br>
                <a href="new_culture.php">Добавить</a><br><br>
                <a href="edit_culture.php">Редактировать</a><br><br>
                <a href="del_culture.php">Удалить</a><br>
                
            </div>
          <?= $footer; ?>
        </div>
    </div>
</body>

</html>

