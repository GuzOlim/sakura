<?php require_once ("global.php"); ?>

<!DOCTYPE html">
<html>

<head>    
    <title>Страница для администратора</title>	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body> 
  <div id="page">
    <?= $header ?>
      <div id="wrapper">
          <div id="content">
            <td valign='top'>
              <form name='form1' method='post' action="add_culture.php">
                <p><label>Введите название города<br>
                    <input type="text" name="title">
                    </label>
                </p> 
                <p><label>Введите дату<br>
                    <input type="date" name="date">
                    </label>
                </p> 
                <p><label>Краткое описание<br>
                    <input type="text" name="meta_d">
                    </label>
                </p> 
                <p><label>Введите полный текст статьи с тэгами<br>
                    <textarea name="text" id="text" cols="50" rows="10"></textarea>
                    </label>
                </p> 
                <p><label>
                    <input type="submit" name="submit" id="submit" value="Добавить">
                    </label>
                </p>
              </form>
            </td>                       
          </div>
        <?= $footer; ?> 
      </div>   
  </div>   
</body>

</html>
