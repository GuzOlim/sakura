<?php
require_once("global.php");
require_once("bd.php"); /*Соединение с базой данных*/
$bd = db_connect();
$query = "SELECT id, title FROM settings";
$result = mysqli_query($bd, $query);
$myrow = mysqli_fetch_array($result, MYSQLI_BOTH);
?>


<!DOCTYPE html">
<html>

<head>
    <title>Страница для администратора</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
    <div id="page">
        <?= $header; ?>
        <div id="wrapper">
            <div id="content">
                <?php
                if (!isset($_GET['id'])) {
                    $query = "SELECT id, title FROM settings";
                    $result = mysqli_query($bd, $query);
                    $myrow = mysqli_fetch_array($result, MYSQLI_BOTH);
                    do {
                        printf("<p><a href='edit_index.php?id=%s'>%s</a></p>", $myrow["id"], $myrow["title"]);
                    } while ($myrow = mysqli_fetch_array($result, MYSQLI_BOTH));
                } else {
                    $id = $_GET['id'];
                    $query = "SELECT * FROM settings WHERE id = $id";
                    $result = mysqli_query($bd, $query);
                    $myrow = mysqli_fetch_array($result, MYSQLI_BOTH);



                    print <<<HERE
<form name='form1' method='post' action='update_index.php'>
<p><label>Введите название<br>
<input value = '$myrow[title]' type="text" name="title">
</label>
</p> 
<p><label>Ключевые слова<br>
<input value = '$myrow[meta_d]' type="text" name="meta_d">
</label>
</p> 
<p><label>Введите полный текст статьи с тэгами<br>
<textarea name="text" id="text" cols="50" rows="10">$myrow[text]</textarea>
</label>
</p> 
<p><select  name="page" required="required">
                    <option value="">Выберите вкладку для создания зоголовка</option>
                    <option value="index">Главная</option>
                    <option value="city">Города</option>
                    <option value="culture">Культура </option>
                    <option value="food">Кухня</option>
                    <option value="news">Новости</option>
                    <option value="sakura">Цветение сакуры</option>
                </select></p>
<input name="id" type="hidden" value="$myrow[id]">
<p><label>
<input type="submit" name="submit" id="submit" value="Редактировать">
</label>
</p>
</form>
HERE;
                }
                ?>

            </div>
            <?= $footer; ?>
        </div>
    </div>
</body>

</html>