<?php
require_once("bd.php"); /*Соединение с базой данных*/
require_once("global.php"); /*соединение с разметкой шапки, меню и подвала*/
$bd = db_connect();

$query = "SHOW TABLES LIKE 'culture';";
$result = mysqli_query($bd, $query);
$myrow = mysqli_fetch_array($result, MYSQLI_BOTH);

if (is_null($myrow)) {
    $query = "CREATE TABLE culture (id INT NOT NULL AUTO_INCREMENT, title VARCHAR(128), meta_d VARCHAR(128), text TEXT, date DATE, PRIMARY KEY (id));";
    $result = mysqli_query($bd, $query);
    if (!$result) throw new Exception("Таблица не создана");
}

$query = "SELECT title,meta_d,text FROM settings WHERE page='culture'";
$result = mysqli_query($bd, $query); // or trigger_error(mysqli_error($bd)." in ".$query);
$myrow = mysqli_fetch_array($result, MYSQLI_BOTH);

if (is_null($myrow)) {
    $myrow = [
        'meta_d' => '',
        'title' => '',
        'text' => '',
    ];
}
?>


<!DOCTYPE html">
<html>

<head>
    <meta name="description" content="<?php echo $myrow['meta_d']; ?> ">
    <title><?php echo $myrow['title']; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>

<body>
    <div id="page">
        <?= $header; ?>
        <div id="wrapper">
            <div id="content">

                <?php echo $myrow['text'];/*вставить текст*/ ?>

                <?php /*таблица с выводом статей*/
                $query = "SELECT id, title, date, text FROM culture";
                $result = mysqli_query($bd, $query);

                while ($myrow = mysqli_fetch_array($result, MYSQLI_BOTH)) { ?>

                    <table align='center' class='stadt'>

                        <tr>
                            <td class='stadt_title''><p class=' stadt_name'><a href='view_culture.php?id=<?= $myrow["id"]; ?>'><?= $myrow["title"]; ?></a></p>
                                <p class='adds'>Дата: <?= $myrow["date"]  ?></p>
                            </td>
                        </tr>

                        <tr>
                            <td><?= $myrow["text"]; ?></td>
                        </tr>

                    </table><br>

                <?php } /*таблица с выводом статей*/ ?>

            </div>
            <?= $footer; ?>
        </div>
    </div>
</body>

</html>