<?php
    require_once("bd.php"); /*Соединение с базой данных*/
    require_once("global.php"); /*соединение с разметкой шапки, меню и подвала*/
    $bd = db_connect();
    $query = "SELECT title,meta_d,text FROM settings WHERE page='sakura'";
    $result = mysqli_query($bd,$query);// or trigger_error(mysqli_error($bd)." in ".$query);
    $myrow = mysqli_fetch_array($result,MYSQLI_BOTH)
?>
    

<!DOCTYPE html">
<html>
<head>    
    <meta name="description" content="<?php echo $myrow['meta_d']; ?> ">
    <title><?php echo $myrow['title']; ?></title>	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
    <div id="page">
       <?= $header; ?>
        <div id="wrapper">
            <div id="content">
                <?php echo $myrow['text'];/*вставить текст*/ ?>              
            </div>
        <?= $footer; ?>
    </div>
</body>
</html>

