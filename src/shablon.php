<!DOCTYPE html">
<head>
<title>Япония - моя любовь</title>	
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>

<div id="page">

<div id="header">
<h1><a href="index.php">Япония</a></h1>
<h2>Страна Восходящего Солнца</h2>
</div>

<div id="menu">
<ul>
<li><a href="index.php">Главная</a></li> 

<li><a href="stadt.php">Города</a></li> 
<li><a href="star.php">Культура</a></li> 
<li><a href="eda.php">Кухня</a></li>
<li><a href="news.php">Новости</a></li> 
<li><a href="sakura.php">Цветение сакуры</a></li> 
</ul>
</div>

<div id="wrapper">

<div id="content">
    
    

<h2>Календарь цветения сакуры</h2>
<div style="float: left; padding: 0 5px 0 0;"><img src="cvet_sakuri.jpg" alt="flower" /></div>
<p>Цветение сакуры - одно из самых красивых событий в Японии. Его ждут большинство японцев, а также многие туристы пытаются успеть на это прекрасное событие. Чтобы не пропутить данное событие для Вас предоставлен календарь цветения сакуры.</p>
<div style="float: left; padding: 0 5px 0 0;"><img src="kalend.jpg" alt="flower" /></div>

    
    
    
</div>

<div id="sidebar"> 

<h2>Каталог</h2>

<ul>
<li><a href="kupi.php">Товары</a></li> 
<li><a href="otdohni.php">Туры</a></li> 
</ul>

<h2>Контакты</h2>
<ul>
<font color="purple">
<p>Г.Барнаул</p>
<p>Ул.Попова, 217</p>
<p>8-(960)939-21-21</p>
</font>
</ul>

</div>

<div style="clear: both;"> </div>

</div>

<div id="footer">
<p>
Сементина Гузаль Тохировна
</p>
</div>

</div>

		
</body>

</html>

